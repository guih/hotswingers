angular
    .module('HotSwingers')
    	.factory('Config', Config);
Config.$inject = ['$http'];

function Config($http){
	return {
		API_URI: 'http://localhost:8081',
		ANALYTICS: 'analytics-id-here',
		MIXPANEL: 'mixpanel-id-here'
	};
}