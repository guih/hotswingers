angular
    .module('HotSwingers')
    	.factory('Accounts', Accounts);
Accounts.$inject = ['$http', 'Config'];

function Accounts($http, Config){
	var vm = {};
	
	vm.tmp = {};

	vm.signIn = function(auth){
		auth.ts = (+new Date());
		return $http
			.post( Config.API_URI + '/accounts/auth/login', auth);
	};

	vm.register = function(data){
		vm.tmp = data;
	};	

	vm.getTemporaryAccount = function(){		
		return vm.tmp ? vm.tmp : {};
	};

	vm.forget = function(){
		return $http.post('/user', {
			_: +new Date()
		});
	}

	return vm;
}