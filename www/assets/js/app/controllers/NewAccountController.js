angular
	.module('HotSwingers')
	.controller('NewAccountController', NewAccountController);

NewAccountController.$inject = [ 
	'$scope', 
	'$interval', 
    'Accounts', 
    'Header'
];

function NewAccountController($scope, $interval, Accounts, Header){
    $scope.images = [];

    $scope.account = Accounts.getTemporaryAccount();

    $scope.user = {};


    $scope.selectFile = function(target){
        setTimeout(function () {
          angular.element(target).trigger('click');
        }, 0);

    };

    $scope.imageUpload = function(event, target){
        $scope.user.images = [];
         var files = event.target.files; 
         
         for (var i = 0; i < files.length; i++) {
             var file = files[i];
                
                var reader = new FileReader();
                
                reader.onload = function(e){
                    $scope.$apply(function() {
                        if( event.target.id == 'file1' ){
                            $scope.images[0] = e.target.result;                            
                        }else{
                            $scope.images[1] = e.target.result;    
                        }
                    });
                }; 
                
                reader.readAsDataURL(file);
         }
    }



    $scope.$watch('user', 
        function() { console.log($scope.user, $scope.account, $scope.images) }, true);  

    $scope.account.userMeta = $scope.user;

    $scope.doAfterNewAccount = function(){        
        Accounts.register(  $scope.account );

        if( $scope.account.type != 'couple' && $scope.images[0] ){
            $scope.account.userMeta.profile1.images = $scope.images[0];
 
        }
        if( $scope.account.type == 'couple' ){
            $scope.account.userMeta.profile1.images = $scope.images[0];
            $scope.account.userMeta.profile2.images = $scope.images[1];
        }

        Header.redirect('couple-data');
    }


}