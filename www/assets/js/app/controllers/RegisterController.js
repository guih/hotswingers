angular
	.module('HotSwingers')
	.controller('RegisterController', RegisterController);

RegisterController.$inject = [
	'$scope',
	'$interval',
    'Accounts',
    'Header'
];

function RegisterController($scope, $interval, Accounts, Header){
    $scope.profile = {};


    var vm = $scope;

    $scope.showMsg = true;
    $scope.$msg = {};
    $scope.profile = {};

    $scope.$watch('profile',
        function() {
            $scope.$msg.msgShow = false;
    }, true);


    vm.register = function(){

        $scope.checkTerms = function(){
            console.log(arguments);
        }

        $scope.showMsg = false;


        Accounts.register($scope.profile);

        Header.redirect('new-account')
    }

}
