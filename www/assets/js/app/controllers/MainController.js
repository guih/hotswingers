angular
	.module('HotSwingers')
	.controller('MainController', MainController);

MainController.$inject = [ 
	'$scope', 
	'$timeout'
	,'$state'
];

function MainController($scope, $timeout, $state){
    
    $scope.text = $scope.menu ? 'close menu':'open menu';


    $scope.toggleMenu = function(){
        $scope.menu = !$scope.menu;
        $scope.text = $scope.menu ? 'close menu':'open menu';
        
    };

    $scope.r = function(route){
    	$state.go(route);
    }
}