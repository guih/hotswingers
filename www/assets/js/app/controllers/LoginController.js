angular
	.module('HotSwingers')
	.controller('LoginController', LoginController);

LoginController.$inject = [ 
	'$scope', 
	'$interval',
	'Accounts',
	'Header'
];

function LoginController($scope, $interval, Accounts, Header){
	var vm = $scope;

    $scope.showMsg = true;
    $scope.$msg = {};
    $scope.login = {};

    $scope.$watch('login', 
    	function() { $scope.$msg.msgShow = false; }, true);
	

    vm.logIn = function(){
    	$scope.showMsg = false;
    	Accounts.signIn($scope.login).then(function(response){
    		var payload = response.data;

    		if( !payload ) return;

    		if( payload.success == true ){
    			console.log('LOGIN OK!', payload);
    			Header.redirect('featured-profile');
    			return;
    		}

    		if( payload.success == false ){
    			console.log('LOGIN NOK!', payload);
    			$scope.$msg.msgShow = true;
    			$scope.$msg.className = 'error';
    			$scope.$msg.msgContent = payload.caption;

    			return;
    		}    		
    	});
    }
}