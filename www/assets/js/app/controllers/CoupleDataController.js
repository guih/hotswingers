angular
	.module('HotSwingers')
	.controller('CoupleDataController', CoupleDataController);

CoupleDataController.$inject = [
	'$scope',
	'$timeout',
    'Accounts',
    'Header'
];

function CoupleDataController($scope, $timeout, Accounts, Header){
    $scope.account = Accounts.getTemporaryAccount();
    $scope.avatar = null;
    $scope.coupleData = {};

    $scope.$watch('coupleData',
        function() {
            console.log($scope.coupleData, $scope.account)
        }, true);


    $scope.selectFile = function(target){
        setTimeout(function () {
          angular.element(target).trigger('click');
        }, 0);

    };

    $scope.imageUpload = function(event, target){
         var files = event.target.files;

         for (var i = 0; i < files.length; i++) {
             var file = files[i];

                var reader = new FileReader();

                reader.onload = function(e){
                    $scope.$apply(function() {
                       $scope.avatar = e.target.result;
                       $scope.coupleData.avatar = $scope.avatar;

                    });
                };

                reader.readAsDataURL(file);
         }
    }

    $scope.proceedToPhotos = function(){

        if( !$scope.coupleData.nick ){
            $scope.coupleData.nick = $scope.account.userMeta.profile1.nick;
        }

        $scope.account.coupleData = $scope.coupleData;

        Accounts.register($scope.account);

        console.log('$$ACOUNT =>', Accounts.getTemporaryAccount());

    }

    $scope.showImage = function(){
        $scope.imageToggle = true;
        $timeout(function(){
            $scope.imageToggle = null;
        }, 2000);
    };
}
