var Api = {};

Api.errors = [];
var returnObject = {};

Api.stdResponse = function(args, fn){
	if( !args.code || !args.caption ){	
		this.strError({type: 'Invalid parameters', })
		return;
	}	

	returnObject = {
		type: args.type || 'unkown',
		caption: args.caption,
		timex: (+new Date()),
		code: args.code,
		success: args.error
	}

	fn(returnObject || {});

}

module.exports = Api;